import { setTimeout } from "node:timers/promises";

export async function test() {
  console.log("Waiting for 1 sec");

  await setTimeout(1000);

  return "Hello world";
}
